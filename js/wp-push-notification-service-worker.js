'use strict';

var href = BASE_URL;
var ajaxHandlerURL = AJAX_HANDLER;
var iconURL = ICON_URL;
var DEBUG = true;

var old_console_log = console.log;
console.log = function () {
    if (DEBUG) {
        old_console_log.apply(this, arguments);
    }
}

self.addEventListener('push', function (event) {
    console.log('Received a push message', event);

    var title = 'Push server error';
    var body = 'Error retrieving data from the server.';
    var icon = iconURL;
    var tag = 'wp-pn-gcm';

    event.waitUntil(
        registration.pushManager.getSubscription()
            .then(function (subscription) {
                console.log(subscription);//subscriptionId


                // fetch subscription id
                console.log(subscription.endpoint);
                var SUBSCRIPTION_ID = subscription.subscriptionId;
                return fetch(ajaxHandlerURL,
                    {
                        method: 'post',
                        headers: {"Content-type": "application/x-www-form-urlencoded;charset=UTF-8"},
                        body: 'action=pngcm_sw&sid=' + subscription.endpoint
                    });
            })
            .then(function (response) {


                if (response.status !== 200) {
                    console.log('Looks like there was a problem. Status Code: ' + response.status);
                    throw new Error();
                }
                // fetch push content
                return response.json();
            })
            .then(function (json) {

                console.log(json);
                if (json.error) {
                    console.log('The API returned an error.', data.error);
                    throw new Error();
                }
                title = json.p_title;
                body = json.p_body;
                icon = json.p_icon;
                tag = json.p_tag;
                href = json.p_href;
                //     return self.registratiinstall(title, {body: body, icon: icon, tag: tag});
                return self.registration.showNotification(title, {body: body, icon: icon, tag: tag});
            }, function (err) {
                console.log(err);
                return self.registration.showNotification(title, {body: body, icon: icon, tag: tag});
            })
    );

});


self.addEventListener('notificationclick', function (event) {
    console.log('On notification click: ', event.notification.tag);
    // Android doesn’t close the notification when you click on it
    // See: http://crbug.com/463146
    event.notification.close();

    // This looks to see if the current is already open and
    // focuses if it is
    event.waitUntil(clients.matchAll({
        type: "window"
    }).then(function (clientList) {
        for (var i = 0; i < clientList.length; i++) {
            var client = clientList[i];
            if (client.url == '/' && 'focus' in client)
                return client.focus();
        }
        if (clients.openWindow)
            return clients.openWindow(href);
    }));

});
