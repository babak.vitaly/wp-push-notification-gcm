var PngcmCommon = {};

(function ($) {


    PngcmCommon = {

        ajaxRequest: function (param, cb) {

            var post_data = $.param(param);
            $.ajax({
                url: pngcmPhpVars.handler_url,
                type: "POST",
                data: post_data,
                dataType: "json"
            }).done(function (data) {
                cb(data);
            });

        }

    };

})(jQuery);