(function ($) {
    $(document).ready(function ($) {

        var selSendForAllCheck = 'input[name="send_for_all"]';
        var selRegID = 'input[name="send_reg_id"]';

        function updateSendForAllCheck() {

            if ($(selSendForAllCheck).prop('checked')) {
                $(selRegID).prop("disabled", true);
            }
            else {
                $(selRegID).prop("disabled", false);
            }
        }

        $(selSendForAllCheck).click(function () {

            updateSendForAllCheck();
            console.log('$(selSendForAllCheck).click');

        });

        updateSendForAllCheck();


    });

})(jQuery);