'use strict';
var PngcmUser;

(function ($) {

    var old_console_log = console.log;
    console.log = function () {
        if (pngcmPhpVars.debug) {
            old_console_log.apply(this, arguments);
        }
    }

    PngcmUser = {

        btnID: '#pngcm-sub-unsub-btn',

        addBtn: function () {

            $('<button id="pngcm-sub-unsub-btn" ></button>').appendTo("body");
        },

        addBtnTxtSub: function () {

            $(this.btnID).html(pngcmPhpVars.t_b_subscribe);

        },

        addBtnTxtUnSub: function () {

            $(this.btnID).html(pngcmPhpVars.t_b_unsubscribe);

        },
        btnSubEn: function () {
            $(this.btnID).prop("disabled", false);
        },
        btnSubDis: function () {
            $(this.btnID).prop("disabled", true);
        },
        btnSubShow: function () {
            $(this.btnID).show();
        },
        btnSubHide: function () {
            $(this.btnID).hide();
        },


        addSubscribeToDB: function (sid, cb) {

            PngcmCommon.ajaxRequest({action: 'pngcm_sub', sid: sid}, cb);

        },

        deleteSubFromDB: function (sid, cb) {

            PngcmCommon.ajaxRequest({action: 'pngcm_unsub', sid: sid}, cb);

        }


    };


    var Sw = {

        isPushEnabled: false,
        endpointID: 0,
        unsubscribe: function () {

            PngcmUser.btnSubDis();


            navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {
                // To unsubscribe from push messaging, you need get the
                // subcription object, which you can call unsubscribe() on.
                serviceWorkerRegistration.pushManager.getSubscription().then(
                    function (pushSubscription) {
                        // Check we have a subscription to unsubscribe
                        if (!pushSubscription) {
                            // No subscription object, so set the state
                            // to allow the user to subscribe to push
                            Sw.isPushEnabled = false;
                            PngcmUser.addBtnTxtSub();
                            PngcmUser.btnSubEn();
                            return;
                        }

                        // TODO: Make a request to your server to remove
                        // the users data from your data store so you
                        // don't attempt to send them push messages anymore

                        // We have a subcription, so call unsubscribe on it
                        pushSubscription.unsubscribe().then(function (successful) {


                            PngcmUser.deleteSubFromDB(Sw.endpointID, function (result) {

                                console.log(result);
                                if (!result['state'])
                                    alert(result['message']);
                                else {
                                    PngcmUser.addBtnTxtSub();
                                    PngcmUser.btnSubEn();
                                    alert(pngcmPhpVars.t_b_unsubscribe_compleat);
                                    Sw.isPushEnabled = false;
                                    console.log('Unsubscription complet')
                                }


                            });


                        }).catch(function (e) {
                            // We failed to unsubscribe, this can lead to
                            // an unusual state, so may be best to remove
                            // the subscription id from your data store and
                            // inform the user that you disabled push

                            console.log('Unsubscription error: ', e);
                            PngcmUser.btnSubEn();
                        });
                    }).catch(function (e) {
                        console.log('Error thrown while unsubscribing from ' +
                        'push messaging.', e);
                    });
            });
        },
        subscribe: function () {
            // Disable the button so it can't be changed while
            // we process the permission request

            console.log('subscribe()');
            PngcmUser.btnSubDis();

            navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {

                console.log('navigator.serviceWorker.ready.then()');


                serviceWorkerRegistration.pushManager.subscribe({userVisibleOnly: true})
                    .then(function (subscription) {
                        // The subscription was successful

                        // TODO: Send the subscription subscription.endpoint
                        // to your server and save it to send a push message
                        // at a later date

                        var endpointID = Sw.getEndpointID(subscription);


                        PngcmUser.addSubscribeToDB(endpointID, function (result) {

                            console.log(result);
                            if (!result['state'])
                                alert(result['message']);
                            else {
                                Sw.isPushEnabled = true;
                                PngcmUser.addBtnTxtUnSub();
                                PngcmUser.btnSubEn();
                                alert(pngcmPhpVars.t_b_subscribe_compleat);
                            }


                            return endpointID;

                        });


                    })
                    .catch(function (e) {
                        if (Notification.permission === 'denied') {
                            // The user denied the notification permission which
                            // means we failed to subscribe and the user will need
                            // to manually change the notification permission to
                            // subscribe to push messages
                            console.log('Permission for Notifications was denied');
                            alert(pngcmPhpVars.t_b_unsubscribe_e_denied);

                        } else {
                            // A problem occurred with the subscription, this can
                            // often be down to an issue or lack of the gcm_sender_id
                            // and / or gcm_user_visible_only
                            console.log('Unable to subscribe to push.', e);

                        }

                        PngcmUser.btnSubEn();
                    });
            });
        },
        initialiseState: function () {




            // Are Notifications supported in the service worker?
            if (!('showNotification' in ServiceWorkerRegistration.prototype)) {
                console.log('Notifications aren\'t supported.');
                return 0;
            }

            // Check the current Notification permission.
            // If its denied, it's a permanent block until the
            // user changes the permission
            if (Notification.permission === 'denied') {
                console.log('The user has blocked notifications.');
                return 0;
            }

            // Check if push messaging is supported
            if (!('PushManager' in window)) {
                console.log('Push messaging isn\'t supported.');
                return 0;
            }


            console.log('navigator.serviceWorker.ready.then(function (serviceWorkerRegistration):');
            navigator.serviceWorker.ready.then(function (serviceWorkerRegistration) {

                console.log('OK');

                serviceWorkerRegistration.pushManager.getSubscription()
                    .then(function (subscription) {
                        // Enable any UI which subscribes / unsubscribes from
                        // push messages.

                        console.log('Enable any UI which subscribes / unsubscribes from');

                        if (!subscription) {
                            // We aren’t subscribed to push, so set UI
                            // to allow the user to enable push
                            console.log('!subscription');

                            PngcmUser.btnSubShow();
                            PngcmUser.addBtnTxtSub();
                            PngcmUser.btnSubEn();

                            return 0;
                        }

                        // Keep your server in sync with the latest subscription
                        console.log('mergedEndpoint:');
                        console.log(Sw.getEndpointID(subscription));

                        PngcmUser.btnSubShow();
                        PngcmUser.addBtnTxtUnSub();
                        PngcmUser.btnSubEn();
                        Sw.isPushEnabled = true;

                        // Set your UI to show they have subscribed for
                        // push messages


                    })
                    .catch(function (err) {
                        console.log('Error during getSubscription()', err);
                    });


            }).catch(function (err) {
                console.log('error:', err);
            });


        },
        getEndpointID: function (pushSubscription) {
            // Make sure we only mess with GCM
            if (pushSubscription.endpoint.indexOf('https://android.googleapis.com/gcm/send') !== 0) {
                return pushSubscription.endpoint;
            }

            var mergedEndpoint = pushSubscription.endpoint;
            // Chrome 42 + 43 will not have the subscriptionId attached
            // to the endpoint.
            if (pushSubscription.subscriptionId &&
                pushSubscription.endpoint.indexOf(pushSubscription.subscriptionId) === -1) {
                // Handle version 42 where you have separate subId and Endpoint
                mergedEndpoint = pushSubscription.endpoint + '/' +
                pushSubscription.subscriptionId;
            }

            var endpointSections = mergedEndpoint.split('/');
            return Sw.endpointID = endpointSections[endpointSections.length - 1];
        },

        init: function () {

            $(PngcmUser.btnID).click(function () {
                if (Sw.isPushEnabled)
                    Sw.unsubscribe();
                else
                    Sw.subscribe();
            });

            if ('serviceWorker' in navigator) {

                navigator.serviceWorker.register(pngcmPhpVars.sw_js_url + '?v=18')
                    .then(function () {
                        console.log('Service worker installed');

                        Sw.initialiseState();
                    });
            } else {
                console.log('Service workers aren\'t supported in this browser.');
                return 0;
            }


        }
    };


    $(window).load(function () {

        PngcmUser.addBtn();
        Sw.init();

    });


})(jQuery);
