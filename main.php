<?php
/*
Plugin Name: Push Notification GCM
Description: Plugin to send push notifications via Google Cloud Messaging.
Version: 1.1
Author: Vitalij Babak
Author URI: http//electro.mk.ua/?p=886
*/

/*  Copyright 2015  Babak Vitalij  (email : babak.vitaly {at} gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


include plugin_dir_path(__FILE__) . 'php/db.php';
include plugin_dir_path(__FILE__) . 'php/view.php';
include plugin_dir_path(__FILE__) . 'php/push.php';
include plugin_dir_path(__FILE__) . 'php/handler.php';
include plugin_dir_path(__FILE__) . 'php/validation.php';
include plugin_dir_path(__FILE__) . 'php/common.php';