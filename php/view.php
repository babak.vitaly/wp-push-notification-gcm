<?

Class PngnmView
{
    static $settingsMessage = '';

    function adminConfig()
    {
        global $wpdb, $pngcmDB, $pngcmCommon, $pngcmPush, $pgcmSettingsMessage;


        $sendData = get_option('pngcm_send_data');
        $settingsData = get_option('pngcm_settings_tmp');

        if ($pgcmSettingsMessage != '')
            $this->pluginSettingsMessage($pgcmSettingsMessage);

        ?>


        <h1><?= $pngcmCommon->pluginFullName ?></h1>
        <div class="wrap" id="pngcm">


            <fieldset class="settings">
                <legend><?php _e("Settings") ?></legend>
                <form method="post">

                    <p>Sender ID</p>

                    <p><input type="text" name="sender_id"
                              value="<?= $settingsData['sender_id'] ?>"/></p>

                    <p>API key</p>

                    <p><input type="text" name="api_key"
                              value="<?= $settingsData['api_key'] ?>"/></p>

                    <p>
                        <span><?php _e("Newsletter when publishing") ?></span>
                        <input type="checkbox" name="send_on_publish"
                            <?= ($settingsData['send_on_publish'] == 'on') ? 'checked="checked"' : '' ?>/>
                    </p>

                    <input type="hidden" name="pngcm_action" value="save_settings"/>

                    <input type="submit" name="update" value="<?php _e("Save") ?>">

                </form>

            </fieldset>


            <fieldset class="newsletter">
                <legend><?php _e("Newsletter") ?></legend>

                <form method="post">


                    <table class="addressee">
                        <tr>
                            <td><p>Reg ID</p></td>
                            <td><p><?php _e("To all") ?></p></td>
                        </tr>

                        <tr>
                            <td><input type="text" name="send_reg_id" value="<?= $sendData['send_reg_id'] ?>"/></td>
                            <td align="center">
                                <input type="checkbox" name="send_for_all"
                                    <?= ($sendData['send_for_all'] == 'on') ? 'checked="checked"' : '' ?>/>
                            </td>
                        </tr>

                    </table>


                    <p><?php _e("Title") ?></p>

                    <p><input type="text" name="send_title" value="<?= $sendData['title'] ?>"/></p>

                    <p><?php _e("Link") ?></p>

                    <p><input type="text" name="send_href" value="<?= $sendData['href'] ?>"/></p>


                    <p><?php _e("Body") ?></p>

                    <p><input type="text" name="send_body" value="<?= $sendData['body'] ?>"></p>


                    <p><?php _e("Icon (link for image)") ?></p>

                    <p><input type="text" name="send_icon" value="<?= $sendData['icon'] ?>"></p>


                    <p><?php _e("Tag") ?>Тег</p>

                    <p><input type="text" name="send_tag" value="<?= $sendData['tag'] ?>"></p>


                    <input type="hidden" name="pngcm_action" value="send"/>

                    <input type="submit" value="<?php _e("Send") ?>">

                </form>


                <p><?php _e("List of subscribers") ?></p>

                <table class="subscription-list">
                    <tr>
                        <th>ID</th>
                        <th>IP</th>
                        <th><?php _e("Date") ?></th>
                        <th><?php _e("Subscriber Reg ID") ?></th>
                    </tr>

                    <?
                    $subscriptions = $pngcmDB->getSubscriptions();

                    if (!isset($subscriptions[0]['id']))
                        echo '<tr><td colspan="4" align="center">' . _("List is blank") . '</td></tr>';
                    foreach ($subscriptions as $subscription) {
                        ?>

                        <tr>
                            <td><?= $subscription['id'] ?></td>
                            <td><?= $subscription['ip'] ?></td>
                            <td align="center"><?= $subscription['date'] ?></td>
                            <td align="center"><input type="text" class="reg-id" size="20"
                                                      value="<?= $subscription['sid'] ?>"></td>
                        </tr>

                    <? } ?>

                </table>

            </fieldset>

        </div>
    <?


    }


    function userButton()
    {

    }

    function pluginSettingsMessage($message, $type = 'normal')
    {
        ?>
        <div id="setting-error-settings_updated" class="updated settings-error">
            <p><strong><?= $message ?></strong></p>
        </div>
    <?
    }


}

$pngnmView = new PngnmView();