<?

Class PngnmCommon
{
    public $pluginFullName = 'Push Notification GCM';
    public $pluginShortName = 'Push Notification';
    public $pluginVersion = '1.1';
    public $phpVar;
    public $pluginPage;


    function __construct()
    {


        global $pngcmHandler, $pngcmCommon, $pngcmPush, $pngcmView, $pgcmSettingsMessage;


        $this->phpVar = array(
            't_b_subscribe' => _('Subscribe for push'),
            't_b_unsubscribe' => _('Unsubscribe from push'),
            't_b_subscribe_compleat' => _('You have successfully subscribed to the push notifications'),
            't_b_unsubscribe_compleat' => _('You have successfully unsubscribed from the push notifications'),
            't_b_unsubscribe_e_denied' => _('You are not allowed to accept browser push notifications'),
            'sw_js_url' => site_url() . '/wp-push-notification-service-worker.js',
            'handler_url' => site_url() . '/wp-admin/admin-ajax.php',
            'debug' => true
        );


        add_action('admin_menu', array(&$this, 'handlerAdmin'));


        if ($this->isPluginDataNeedUpdate()) return 0;
        //  if (!$this->checkManifest()) return 0;
        if (!$this->is_https()) return 0;


        add_action('wp_head', array(&$this, 'hookHead'));


        $this->loadCommonJS();

        if (!is_admin()) {
            $this->userPage();
        }


        add_action('wp_ajax_pngcm_sub', array(&$pngcmHandler, 'subscribe'));
        add_action('wp_ajax_nopriv_pngcm_sub', array(&$pngcmHandler, 'subscribe'));

        add_action('wp_ajax_pngcm_unsub', array(&$pngcmHandler, 'unsubscribe'));
        add_action('wp_ajax_nopriv_pngcm_unsub', array(&$pngcmHandler, 'unsubscribe'));

        add_action('wp_ajax_pngcm_sw', array(&$pngcmHandler, 'serviceWorker'));
        add_action('wp_ajax_nopriv_pngcm_sw', array(&$pngcmHandler, 'serviceWorker'));


        add_action("new_to_publish", array(&$pngcmCommon, 'handlerPostPublish'));
        add_action("pending_to_publish", array(&$pngcmCommon, 'handlerPostPublish'));
        add_action("draft_to_publish", array(&$pngcmCommon, 'handlerPostPublish'));
        add_action("auto-draft_to_publish", array(&$pngcmCommon, 'handlerPostPublish'));
        add_action("future_to_publish", array(&$pngcmCommon, 'handlerPostPublish'));
        add_action("private_to_publish", array(&$pngcmCommon, 'handlerPostPublish'));
        add_action("inherit_to_publish", array(&$pngcmCommon, 'handlerPostPublish'));
        add_action("trash_to_publish", array(&$pngcmCommon, 'handlerPostPublish'));


    }


    function is_https()
    {

        $re = "/^https:\\/\\/.*$/";
        if (preg_match($re, site_url())) return 1;
        return 0;

    }

    function loadCommonJS()
    {

        wp_enqueue_script('jquery');
        wp_enqueue_script('pngcm-common', plugins_url('js/common.js', dirname(__FILE__)));
        wp_localize_script('pngcm-common', 'pngcmPhpVars', $this->phpVar);

    }

    function loadUserJS()
    {

        global $pngcmPush;
        if ($pngcmPush->checkManifest())
            wp_enqueue_script('pngcm-user', plugins_url('js/user.js', dirname(__FILE__)));

    }

    function loadPluginJS()
    {

        wp_enqueue_script('pngcm-user', plugins_url('js/admin.js', dirname(__FILE__)));

    }

    function loadPluginStyle()
    {

        $style = plugins_url('css/admin.css', dirname(__FILE__));
        wp_register_style('admin', $style);
        wp_enqueue_style('admin');

    }

    function loadUserStyle()
    {

        $style = plugins_url('css/user.css', dirname(__FILE__));
        wp_register_style('admin', $style);
        wp_enqueue_style('admin');

    }


    function hookHead()
    {

        if (!is_admin()) {

            global $pngcmPush;
            $url = plugins_url('manifest.json', dirname(__FILE__));
            echo '<link rel="manifest" href="' . $url . '">';
        }

    }


    function handlerAdmin()
    {

        $this->pluginPage = add_options_page($this->pluginFullName,
            $this->pluginShortName, 8, basename(__FILE__),
            array(&$this, 'pluginPageBody'));
        add_action('admin_print_scripts-' . $this->pluginPage, array(&$this, 'handlerPluginPage'));

    }


    function pluginPageBody()
    {

        global $pngnmView, $pngcmPush, $pgcmSettingsMessage;

        if (isset($_POST['pngcm_action'])) {

            switch ($_POST['pngcm_action']) {

                case 'send':
                    $this->pluginActionSendPush();
                    break;

                case 'save_settings':
                    $this->pluginActionUpdateSettings();
                    break;

            }
        }


        if (!$pngcmPush->checkManifest()) {
            $pgcmSettingsMessage .= _("File 'manifest.json' is not found, to create it, click 'Save'. ");
        }

        if (!$this->is_https()) {
            $pgcmSettingsMessage .= _("You use the HTTP protocol to correctly plugin work, you must go to the") . " HTTP<b>S</b>. ";
        }


        $pngnmView->adminConfig();
    }


    function userPage()
    {

        $this->loadUserJS();
        $this->loadUserStyle();

    }


    function handlerPluginPage()
    {

        $this->installPluginData();
        $this->loadPluginJS();
        $this->loadPluginStyle();

    }


    function pluginActionUpdateSettings()
    {

        global $pngnmView, $pngcmPush, $pngcmDB, $pngcmValidation, $pgcmSettingsMessage;
        $errorMessage = "";
        $settings = get_option('pngcm_settings_tmp');

        $pngcm_send = $pngcmValidation->request('sender_id', 'Sender ID', '/^[0-9]{3,100}$/');
        if (!$pngcm_send['state'])
            $errorMessage .= $pngcm_send['message'];
        $settings['sender_id'] = $pngcm_send['value'];

        $pngcm_send = $pngcmValidation->request('api_key', 'API key', '/^[a-z0-9\-\_]{3,100}$/i');
        if (!$pngcm_send['state'])
            $errorMessage .= $pngcm_send['message'];
        $settings['api_key'] = $pngcm_send['value'];

        $pngcm_send = $pngcmValidation->request('send_on_publish', _("Newsletter at the publication"), '/(^(on)$)|(^()$)/');
        if (!$pngcm_send['state'])
            $errorMessage .= $pngcm_send['message'];
        $settings['send_on_publish'] = $pngcm_send['value'];

        update_option('pngcm_settings_tmp', $settings);

        if ($errorMessage != '') {
            $pgcmSettingsMessage .= $errorMessage;
            return 0;
        }

        update_option('pngcm_settings_valid', $settings);

        $pngcmPush->updateManifest($settings['sender_id']);

        $pgcmSettingsMessage .= _('Settings have been saved.');

    }


    function installDemoContent()
    {

        //   var_export(get_option('pngcm_settings_tmp'));

        if (get_option('pngcm_settings_tmp') === false) {

            $settingsDefault = array(
                'sender_id' => '652637306577',
                'api_key' => 'AIzaSyBgmbiLIbfTb0rPvQ1AXIYKCmlicPD637I',
                'send_on_publish' => 'on',
                'version' => $this->pluginVersion
            );

            update_option('pngcm_settings_tmp', $settingsDefault);
            update_option('pngcm_settings_valid', $settingsDefault);

        }


        if (get_option('pngcm_send_data') === false) {


            $sendDataDefault = array(
                'title' => _('Push message title'),
                'href' => 'https://electro.mk.ua/?p=886',
                'body' => _('Push message body'),
                'icon' => 'https://electro.mk.ua/wp-content/plugins/wp-push-notification-gcm/img/icon-192x192.png',
                'tag' => 'wp-pn-gcm',
                'send_for_all' => 'on',
                'send_reg_id' => 'APA91bEwfFqiWcfAk3S_bP-yqLm3EAIBlLQ8VURdUaZZquDR7rf8MKb08cqJxNJy-NSpk9vHYL6I01n-mJmFlVYdaGO2afjJfwITl7L6qjofIg0i-6fu40xNwjI9D6CTBhhAaOC5-f-7mPq2htaOLQB2DQPGcs8mfA'
            );

            update_option('pngcm_send_data', $sendDataDefault);

        }


    }

    function pluginActionSendPush()
    {

        global $pngnmView, $pngcmPush, $pngcmDB, $pngcmValidation, $pgcmSettingsMessage;
        $errorMessage = "";

        $sendData = get_option('pngcm_send_data');

        $pngcm_send = $pngcmValidation->request('send_title', _('Title'), '/^.{3,100}$/');
        if (!$pngcm_send['state'])
            $errorMessage .= $pngcm_send['message'];
        $sendData['title'] = $pngcm_send['value'];


        $pngcm_send = $pngcmValidation->request('send_href', _('Link'), $pngcmValidation->regExURL);
        if (!$pngcm_send['state'])
            $errorMessage .= $pngcm_send['message'];
        $sendData['href'] = $pngcm_send['value'];


        $pngcm_send = $pngcmValidation->request('send_body', _('Body'), '/^.{3,300}$/');
        if (!$pngcm_send['state'])
            $errorMessage .= $pngcm_send['message'];
        $sendData['body'] = $pngcm_send['value'];


        $pngcm_send = $pngcmValidation->request('send_icon', _('Icon (link for image)'), $pngcmValidation->regExURL);
        if (!$pngcm_send['state'])
            $errorMessage .= $pngcm_send['message'];
        $sendData['icon'] = $pngcm_send['value'];


        $pngcm_send = $pngcmValidation->request('send_tag', _('Tag'), '/^[a-z0-9\\-]{0,100}$/i');
        if (!$pngcm_send['state'])
            $errorMessage .= $pngcm_send['message'];
        $sendData['tag'] = $pngcm_send['value'];


        $pngcm_send = $pngcmValidation->request('send_for_all', _('To all'), '/(^(on)$)|(^()$)/');
        if (!$pngcm_send['state'])
            $errorMessage .= $pngcm_send['message'];
        $sendData['send_for_all'] = $pngcm_send['value'];


        if ($sendData['send_for_all'] != 'on') {

            $pngcm_send = $pngcmValidation->request('send_reg_id', _('Subscriber Reg ID'), '/.{1,500}/i');
            if (!$pngcm_send['state'])
                $errorMessage .= $pngcm_send['message'];
            $sendData['send_reg_id'] = $pngcm_send['value'];

        }

        update_option('pngcm_send_data', $sendData);


        if ($errorMessage != '') {
            $pgcmSettingsMessage .= $errorMessage;
            return 0;
        }


        if ($sendData['send_for_all'] == 'on')
            $regIDs = $pngcmDB->getSubscriptionsIDs();
        else $regIDs = array($sendData['send_reg_id']);

        $result = json_decode($pngcmPush->sendNotify($regIDs, $sendData));

        if (isset($result->success)) {
            $resultMessage =
                _("Sending successful. ") .
                _("Sent: ") . $result->success . " " .
                _("Not sent: ") . "{$result->failure}.";
        } else $resultMessage = _("Sending message(s) error");

        $pgcmSettingsMessage .= $resultMessage;

    }


    function isPluginDataNeedUpdate()
    {


        $settings = get_option('pngcm_settings_valid');
        if ($settings !== false &&
            $settings['version'] == $this->pluginVersion
        )
            return 0;
        return 1;
    }

    function installPluginData()
    {

        global $pngnmView, $pngnmCommon;

        if (!$this->isPluginDataNeedUpdate()) return 0;

        global $pngcmPush, $pngcmDB;
        $pngcmPush->installServiceWorkerScript();
        $pngcmDB->installTable();
        $this->installDemoContent();

        $settings = get_option('pngcm_settings_valid');
        $settings['version'] = $this->pluginVersion;
        update_option('pngcm_settings_valid', $settings);

        $pngnmCommon->settingsMessage .= "Plug-in configuration Been updated.";

    }

    function handlerUninstall()
    {

        global $pngcmPush;
        $pngcmPush->uninstallServiceWorkerScript();

    }


    function handlerPostPublish($postID)
    {

        global $pngcmDB, $pngcmPush;

        $settings = get_option('pngcm_settings_valid');
        if ($settings['send_on_publish'] != 'on')
            return 0;

        $post = get_post($postID, ARRAY_A);
        $regIDs = $pngcmDB->getSubscriptionsIDs();


        $thumbnailID = get_post_thumbnail_id($post['ID']);
        if ($thumbnailID) {
            $thumbnail = wp_get_attachment_image_src($thumbnailID, 'thumbnail');
            $thumbnail = $thumbnail[0];
        } else {
            $thumbnail = plugins_url('img/icon-192x192.png', dirname(__FILE__));
        }

        $SendData = array(
            'title' => _('Have added a new post'),
            'href' => $post['guid'],
            'body' => _('Post title') . $post['post_title'],
            'icon' => $thumbnail,
            'tag' => 'wp-push-notification-gcm'

        );

        $pngcmPush->sendNotify($regIDs, $SendData);

    }

}


$pngcmCommon = new PngnmCommon();