<?

Class PngnmPush
{

    public $serviceWorkerJsPluginPath;
    public $serviceWorkerJsWpPath;
    public $manifestPath;

    public $gcmURL = "https://android.googleapis.com/gcm/send";
    public $apiKey;


    function __construct()
    {

        $this->manifestPath = plugin_dir_path(dirname(__FILE__)) . "manifest.json";

        $settings = get_option('pngcm_settings_valid');

        $this->serviceWorkerJsPluginPath = plugin_dir_path(dirname(__FILE__)) . 'js/wp-push-notification-service-worker.js';
        $this->serviceWorkerJsWpPath = ABSPATH . 'wp-push-notification-service-worker.js';
        $this->apiKey = $settings['api_key'];

    }

    function updateManifest($senderID)
    {

        global $pngcmCommon;
        $icon = str_replace('http://', '//', plugins_url('img/icon-192x192.png', dirname(__FILE__)));
        $manifest = array(
            'name' => $pngcmCommon->pluginFullName,
            'short_name' => $pngcmCommon->pluginShortName,
            'icons' => array(array('src' => $icon)),
            'start_url' => './index.html?homescreen=1',
            'display' => 'standalone',
            'gcm_sender_id' => $senderID,

            //gcm_user_visible_only is only needed until Chrome 44 is in stable
            'gcm_user_visible_only' => true
        );


        $json = str_replace('\\', '', json_encode($manifest));

        $fp = fopen($this->manifestPath, "w+");
        $result = fwrite($fp, $json);
        fclose($fp);
        return $result;

    }

    function checkManifest()
    {

        return is_file($this->manifestPath);

    }

    function installServiceWorkerScript()
    {

        $ajaxHandlerPath = site_url().'/wp-admin/admin-ajax.php';
        $imgPath = plugins_url('img/icon-192x192.png', dirname(__FILE__));
        $siteUrl = site_url();
        $inputScript = '';

        $fp = fopen($this->serviceWorkerJsPluginPath, 'rt');
        if(!$fp) return 0;

        while (!feof($fp))
            $inputScript .= fgets($fp, 1023);
        fclose($fp);


        $outputScript = str_replace(
            array('BASE_URL', 'AJAX_HANDLER', 'ICON_URL' ),
            array("'$siteUrl'","'$ajaxHandlerPath'", "'$imgPath'"),
            $inputScript);

        $fp = fopen($this->serviceWorkerJsWpPath, 'w+');
        $writeState = fwrite($fp, $outputScript); // Запись в файл
        fclose($fp);

        return $writeState;

    }

    function uninstallServiceWorkerScript()
    {

        return unlink($this->serviceWorkerJsWpPath);

    }

    function sendNotify($reg_ids, $data)
    {

        global $pngcmDB;

        if(!$pngcmDB->addPushes($reg_ids, $data))
            return 0;

        $fields = array(
            'registration_ids' => $reg_ids,
            'data' => $data//array("message" => $message),
        );

        $headers = array(
            'Authorization: key=' . $this->apiKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->gcmURL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Problem occurred: ' . curl_error($ch));
        }

        curl_close($ch);
        return $result;

    }


}

$pngcmPush = new PngnmPush();