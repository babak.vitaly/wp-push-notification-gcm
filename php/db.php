<?

class PngcmDB
{

    static $tableSubscription, $tablePushes;

    function __construct()
    {

        global $wpdb;
        $this->tableSubscription = $wpdb->prefix . "pngcm_subscription";
        $this->tablePushes = $wpdb->prefix . "pngcm_pushes";

    }

    function installTable()
    {

        global $wpdb;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');


        if ($wpdb->get_var("SHOW TABLES LIKE '{$this->tableSubscription}'") != $this->tableSubscription) {

            $sql =
                "CREATE TABLE {$this->tableSubscription}(" .
                "id int UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT," .
                "ip VARCHAR(32) NOT NULL," .
                "date timestamp NOT NULL," .
                "sid VARCHAR(324) NOT NULL)" .
                "ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";


            dbDelta($sql);
            $wpdb->query("ALTER TABLE {$this->tableSubscription} ADD CONSTRAINT unique_sid UNIQUE (sid);");

        }

        if ($wpdb->get_var("SHOW TABLES LIKE '{$this->tablePushes}'") != $this->tablePushes) {


            $sql =
                "CREATE TABLE {$this->tablePushes}(" .
                "id int UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT," .
                "date timestamp NOT NULL," .
                "sid VARCHAR(324) NOT NULL," .
                "p_title VARCHAR(1000) NOT NULL," .
                "p_href VARCHAR(500) NOT NULL," .
                "p_body VARCHAR(3000) NOT NULL," .
                "p_icon VARCHAR(500) NOT NULL," .
                "p_tag VARCHAR(100))" .
                "ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;";

            dbDelta($sql);
        }

    }

    function getSubscriptions()
    {

        global $wpdb;
        $sql = "SELECT * FROM {$this->tableSubscription}";
        return $wpdb->get_results($sql, ARRAY_A);

    }

    function getSubscriptionsIDs()
    {

        global $wpdb;
        $sql = "SELECT sid FROM {$this->tableSubscription}";
        $dbIDs = $wpdb->get_results($sql, ARRAY_A);

        $result = array();
        foreach ($dbIDs as $dbID) {
            $result[] = $dbID['sid'];
        }
        return $result;

    }

    function addSubscription($sid)
    {

        global $wpdb;
        $sqlStr = "INSERT {$this->tableSubscription} " .
            "SET ip = '%s', date = NOW(), sid = '%s'";

        $sql = $wpdb->prepare($sqlStr, $_SERVER['REMOTE_ADDR'], $sid);
        return $wpdb->query($sql);

    }

    function deleteSubscription($sid)
    {

        global $wpdb;
        $sqlStr = "DELETE FROM {$this->tableSubscription} " .
            "WHERE sid = '%s'";

        $sql = $wpdb->prepare($sqlStr, $sid);
        return $wpdb->query($sql);

    }

    function getPush($sid)
    {

        global $wpdb;


        $sqlStr =
            "SELECT * FROM {$this->tablePushes} " .
            "WHERE sid = '%s' " .
            "ORDER BY id LIMIT 1";
        $sql = $wpdb->prepare($sqlStr, $sid);
        $data = $wpdb->get_results($sql, ARRAY_A);

        if (!isset($data[0]['id']))
            return 0;

        $data = $data[0];

        $sqlStr =
            "DELETE FROM {$this->tablePushes} " .
            "WHERE sid = '%s' " .
            "ORDER BY id LIMIT 1";
        $sql = $wpdb->prepare($sqlStr, $sid);
        $wpdb->query($sql);

        return $data;

    }


    function addPushes($sids, $data)
    {


        global $wpdb;

        foreach ($sids as $sid) {

            $sqlStr = "INSERT {$this->tablePushes} " .
                "SET date = NOW(), sid = '%s', " .
                "p_title = '%s', p_href = '%s', " .
                "p_body = '%s', p_icon = '%s', p_tag = '%s'";
            $sql = $wpdb->prepare(
                $sqlStr, $sid,
                $data['title'], $data['href'],
                $data['body'], $data['icon'], $data['tag']
            );
            if (!$wpdb->query($sql))
                return 0;
        }

        return 1;


    }


}

$pngcmDB = new PngcmDB();