<?

class PngcmValidation
{

    public $regExURL = '#((https)://(\S*?\.\S*?))([\s)\[\]{},;"\':<]|\.\s|$)#i';

    function request($arrayKey, $valueName, $regEx = 0)
    {

        $input = $_REQUEST[$arrayKey];
        $input = trim($input);

        if ($regEx !== 0 && !preg_match($regEx, $input))
            return array(
                'state' => 0, 'message' => "Поле '$valueName' заполнено некорректно. ",
                'value' => $input);

        $input = htmlspecialchars($input);

        return array('state' => 1, 'message' => '', 'value' => $input);

    }

}

$pngcmValidation = new PngcmValidation();