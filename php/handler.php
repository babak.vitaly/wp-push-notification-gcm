<?

Class PngcmHandler
{


    function subscribe()
    {


        global $pngcmDB;
        $sid = $_POST['sid'];
        $result = array('state' => 1, 'message' => 'OK');

        if (!$pngcmDB->addSubscription($sid))
            $result = array('state' => 0, 'message' => 'DB Error');

        echo json_encode($result);
        exit;

    }

    function unsubscribe()
    {

        global $pngcmDB;
        $sid = $_POST['sid'];
        $result = array('state' => 1, 'message' => 'OK');

        if (!$pngcmDB->deleteSubscription($sid))
            $result = array('state' => 0, 'message' => 'DB Error');

        echo json_encode($result);
        exit;
    }

    function serviceWorker()
    {

        global $pngcmDB;
        $sid = str_replace ( "https://android.googleapis.com/gcm/send/" , "" , $_REQUEST['sid'] );
        $sendData = $pngcmDB->getPush($sid);
        echo json_encode($sendData);
        exit;
    }


}

$pngcmHandler = new PngcmHandler();



